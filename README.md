# Repository for Algorithms Library Toolkit CI images

Every image has its own directory. If you want autobuild, then add the following lines into `.gitlab-ci.yml` file:

```
deploy:<dirname>:
  <<: *docker_template
```
